import React, { Component } from "react";
import { Nav, Navbar, Form, FormControl, Button } from "react-bootstrap";
import { BrowserRouter as Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

export default class NavBar extends Component {
  render() {
    return (
      <div className="container-fluid" style={{ padding: "0"}}>
        <Navbar
          bg="light"
          expand="lg"
        >
          <Navbar.Brand as={Link} to="/">
            AMS
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto" style={{marginLeft:"30px"}}>
              <Nav.Link as={Link} to="/">
                HOME
              </Nav.Link>
            </Nav>
            <Form inline>
              <FormControl
                type="text"
                placeholder="Search"
                style={{marginRight:"10px"}}
              />
              <Button variant="outline-dark">Search</Button>
            </Form>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}
