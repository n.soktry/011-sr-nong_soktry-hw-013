import React, { Component } from "react";
import { Table } from "react-bootstrap";
import Axios from "axios";
import List from "../List";
import { BrowserRouter as Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";


export default class Homepage extends Component {
  constructor() {
    super();
    this.substring = this.substring.bind(this);
    this.delete = this.delete.bind(this);
    this.state = {
      data: [],
      updating: 1,
    };
  }

  substring = (att) => {
    let year = att.substring(0, 4);
    let month = att.substring(4, 6);
    let day = att.substring(6, 8);
    let Date = [year, month, day];
    return Date.join("-");
  };

  delete = (id, index) => {
    Axios.delete(
      `http://110.74.194.124:15011/v1/api/articles/${id}
          `
    )
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
    alert("you have deleted successfully!");
    var dd = [...this.state.data];
    if (index !== -1) {
      dd.splice(index, 1);
      this.setState((prevState) => {
        return (this.state.data = dd);
      });
    }
  };

  componentWillMount() {
    Axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15")
      .then((res) => {
        this.setState({
          data: res.data.DATA,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  render() {
    const list = this.state.data.map((d, index) => (
      <List
        data={d}
        getDelete={this.getDelete}
        delete={this.delete}
        index={index}
        key={d.ID}
      />
    ));

    return (
      <div className="col-lg-12">
        <div style={{textAlign:"center"}}>
          <h2>Article Management Homework</h2>
          <Link as={Link} to="/add">
            <button className="btn btn-dark m-3">Create Article</button>
          </Link>
        </div>

        <Table responsive striped bordered hover>
          <thead>
            <tr>
              <th>ID</th>
              <th>Title</th>
              <th>Desription</th>
              <th width="9%">Created Date</th>
              <th width="10%">Image</th>
              <th width="20%">Action</th>
            </tr>
          </thead>
          <tbody>{list}</tbody>
        </Table>
      </div>
    );
  }
}
